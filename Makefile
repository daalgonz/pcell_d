CC = mpicc
#CFLAGS = -g -O3 -pg
CFLAGS = -Wall -g -O3 -fopenmp
#CFLAGS = -g -O2 -Wall
STATIC =

#STATIC = -static

#LIBS = -lforms -L/usr/X11R6/lib -lX11 -lXext -lXpm -lm -pg
LIBS = -lm -pg

convection:
	$(CC) $(CFLAGS) $(STATIC) main.c potinc.c plasma_convection.c create_gnuplot.c -o pcell_d $(LIBS)

clean:
	rm -rf pcell_d *.o *~ \#*

clean_data:
	rm -rf *.DAT *.pbm *.ppm
