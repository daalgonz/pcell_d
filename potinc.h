/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   potinc.h
 * Author: okeanos
 *
 * Created on June 25, 2018, 6:23 PM
 */

#ifndef POTINC_H
#define POTINC_H

#ifdef __cplusplus
extern "C" {
#endif

    
#include <stdio.h>
#include <string.h>
#include "omp.h"
#include "mpi.h"

    int  di, dj;
    int i, j, n_t, thr;
    double dx, dy, c, param, reyn;
    float **pcell0, **test, **test1;
    void potinc();



#ifdef __cplusplus
}
#endif

#endif /* POTINC_H */

