#include <stdio.h>


#ifndef CREATE_GNUPLOT_H
#define CREATE_GNUPLOT_H

#ifdef __cplusplus
extern "C" {
#endif


void create_gnuplot(int);

#ifdef __cplusplus
}
#endif

#endif /* CREATE_GNUPLOT_H */

