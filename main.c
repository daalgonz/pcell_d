

#include <stdio.h>
#include <stdlib.h>
#include "potinc.h"
#include "plasma_convection.h"
#include "create_gnuplot.h"


int main(int argc, char** argv) {

   
   if (argc != 7)
    {
        printf("Insufficient arguments, must be: \n ./pcell_d size_x size_y time reyn param thr\n");
        
        return 1;
    }
    
    i = atoi(argv[1]);
    j = atoi(argv[2]);
    n_t = atoi(argv[3]);
    reyn = atof(argv[4]);
    param = atof(argv[5]);
    thr = atoi(argv[6]);
    
    /*i = 100;
    j = 100;
    n_t = 100;
    reyn = 800;
    param = 0.25;
    */
    
    
    omp_set_dynamic(0);
    omp_set_num_threads(thr);
    
    
    int count;
    
    pcell0 = (float **)malloc((i+1) * sizeof(float *));
    test = (float **)malloc((i+1) * sizeof(float *));
    test1 = (float **)malloc((i+1) * sizeof(float *));
    
    for (count=0; count<=i; count++){
        pcell0[count] = (float *)malloc((j+1) * sizeof(float));
        test1[count] = (float *)malloc((j+1) * sizeof(float));
        test[count] = (float *)malloc((j+1) * sizeof(float));
    }
    
    a = (float ***)malloc(10 * sizeof(float **));
    
    for (count = 0; count<10; count++){
    
        a[count] = (float **)malloc((i+1) * sizeof(float *));
        for(di = 0; di <= i; di++)
        {
            a[count][di] = (float *)malloc((j+1) * sizeof(float));
        }
    }
    
    potinc();
        
    convec_plasma();
    
    create_gnuplot(n_t);
    
    system("gnuplot -background white prog_gnuplot");
    
    for (count=0; count<=i; count++){
        free(pcell0[count]);
        free(test1[count]);
        free(test[count]);
    }
    
    for (count=0; count<10; count++){
        
        for(di = 0; di <= i; di++){
        
            free(a[count][di]);
            
        }
        
        free(a[count]);
        
    }
    
    free(a);
    
    free(pcell0);
    free(test);
    free(test1);
    
    return (EXIT_SUCCESS);
}

