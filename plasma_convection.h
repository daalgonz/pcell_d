/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   plasma_convection.h
 * Author: okeanos
 *
 * Created on June 26, 2018, 10:44 AM
 */

#ifndef PLASMA_CONVECTION_H
#define PLASMA_CONVECTION_H

#ifdef __cplusplus
extern "C" {
#endif

#include "potinc.h"
#include <math.h>
#include <stdlib.h>

    void convec_plasma();
    void convect(int);
    void mean_field();
    double re, dt, d_0, la, ta, ka, na, ex, ye, l, m, fa, ca, cb, fa, cd, cda, cdb, cc, fe, fi;
    double mu, md, mt, mc, ms, qac, qbc, qcc, qdc;
    double pau, peu, piu, pad, ped, pid, rau, reu, riu, rou, rad, red, rid, rod, qa, qb, qc, qd;
    float ***a;
    
    
#ifdef __cplusplus
}
#endif

#endif /* PLASMA_CONVECTION_H */

