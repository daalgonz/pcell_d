
#include "potinc.h"


void potinc()
{
    
    
    dx = (float) 1.0/i;
    dy = (float) 1.0/j;
    c = (float) 1.0;
    
    
    
    pcell0[0][0] = 10.0;
    pcell0[0][1] = 10.0;
    pcell0[1][0] = pcell0[0][0] - dx * c;
    pcell0[1][1] = pcell0[0][1] - dx * c;
    test[0][0] = 10.0;
    test[0][1] = 10.0;
    test[1][0] = test[0][0] - dx * c;
    test[1][1] = test[0][1] - dx * c;
    test1[0][0] = 10.0;
    test1[0][1] = 10.0;
    test1[1][0] = test1[0][0] - dx * c;
    test1[1][1] = test1[0][1] - dx * c;
    
 #pragma omp parallel for private(di, dj)
    for ( dj=0; dj<=1; dj+=1)
    {
        for( di=0; di<=i; di+=2)
        {
            pcell0[di][dj] = pcell0[0][dj] - (float)di * dx *c;
            test[di][dj] = test[0][dj] - (float)di * dx *c;
            test1[di][dj] = test1[0][dj] - (float)di * dx *c;
        }
        
        for( di=1; di<=i; di+=2)
        {
            pcell0[di][dj] = pcell0[1][dj] - (float)(di-1) * dx * c;
            test[di][dj] = test[1][dj] - (float)(di-1) * dx * c;
            test1[di][dj] = test1[1][dj] - (float)(di-1) * dx * c;
        }
    }
    
    #pragma omp parallel for private(di, dj)
    for( di=0; di<=i; di+=1)
    {
        for( dj=2; dj<=j; dj+=2)
        {
            pcell0[di][dj] = pcell0[di][0];
            test[di][dj] = test[di][0];
            test1[di][dj] = test1[di][0];
        }
        
        for( dj=1; dj<=j; dj+=2)
        {
            pcell0[di][dj] = pcell0[di][1];
            test[di][dj] = test[di][1];
            test1[di][dj] = test1[di][1];
        }
    }
    
    FILE *pcell = fopen("PCELL0.DAT", "w");
    
    for(di = 0; di <= i; di++){
        for(dj = 0; dj <= j; dj++){
            fprintf(pcell, " %11.8f \n", pcell0[di][dj]);
            
            if(dj == j)
                fprintf(pcell, "\n");
        }
    }
        
    
    fclose(pcell);
        
}