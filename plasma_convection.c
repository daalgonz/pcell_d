#include "plasma_convection.h"

void convec_plasma(){

    int n;
    for(n=1; n<n_t; n++)
        convect(n);

        
}

void convect(int nf) {
    
    //double dx, dy;
    
    dx = 1.0f / i;
    dy = 1.0f / j;
    
    re = 1.0f/reyn;
    if (n_t < 100){
        dt = 0.01;
    }else{
        dt = 1.0f/n_t;
    }
    
    float vx[i+1][j+1], vy[i+1][j+1];
    
   
#pragma omp parallel for collapse(2) private(di, dj, d_0, la, ta, ka, na)
    for (di = 0; di <= i; ++di){
        for(dj = 0; dj <= j; ++dj){
            //////////////////////////////
            a[0][di][dj] = test1[di][dj];
            a[1][di][dj] = test[di][dj];
            //////////////////////////////
            
            d_0 = di * dx - 0.5;
            la = (1.0 - param) * (d_0 * d_0) * 4.0 + param;

            d_0 = dj * dy - 0.5;
            ta = 1.0 - d_0 * d_0 * 4.0;
            ka = cos((di * dx - 0.5) * M_PI);
            na = sin((di * dx - 0.5) * M_PI);

            d_0 = ta;
            
            vx[di][dj] = la * (8.0 / M_PI) * (d_0 * (d_0 * d_0)) * (dj * dy - 0.5) * ka;
            
            d_0 = ta, d_0 *= d_0;
            vy[di][dj] = d_0 * d_0 * (1.0 / (4.0 * M_PI)) 
                * (- M_PI * la * na + (1.0 - param) * 8.0 * ka * (di * dx - 0.5));
            
        }
    }
    
    
    
    
    ex = dt / (dx * 12);
    ye = dt / (dy * 12);
    
    d_0 = dx;
    l = re * dt / (d_0 * d_0);
    
    d_0 = dy;
    m = re * dt / (d_0 * d_0);
    
    int n, z, cs, s, cu, can;
    
    for(n = 0; n <= 4; n++) {
        
        z = 1;
        cs = 1;
        
#pragma omp parallel for collapse(2) private(di, dj, fa, can, s, z, cu, mu, md, mt, mc, ms, qac, qbc, qcc, qdc, pau, peu, piu, pad, ped, pid, rau, reu, riu, rou, rad, red, rid, rod, qa, qb, qc, qd, ca, cb, cd, cc, cda, cdb, fe, fi)
        for(di = 0; di <= i; di++){
            for(dj = 1; dj <= j-1; dj++){
            
                fa = 0.0;
                
                for(can = 1; can <= 2; can++) {
                
                    if(can == 1){
                        s = -1;
                        z = 1;
                    } else {
                        s = 1;
                        z = 0;
                    }
                
                
                    cu = 0;
                    mu = (double) (di + s);
                    md = (double) (di - 2);
                    mt = (double) (di + 2);
                    mc = (double) (dj - 2);
                    ms = (double) (dj + 2);
                    qac = mt - 1;
                    qbc = md + 1;
                    qcc = ms - 1;
                    qdc = mc + 1;
                
                    if(di == i && s == 1){
                        mu = 0.0;
                        cu = 1;
                    }
                
                    if(di == 0 && s == -1){
                        mu = 0.0;
                        cu = 2;
                    }
                
                    if(di == 0){
                        md = 0.0;
                        qbc = 0.0;
                    }
                    
                    if(di == 1){
                        md = 0.0;
                    }
                
                    if(di == i){
                        mt = 0.0;
                        qac = 0.0;
                    }
                
                    if(di == i-1){
                        mt = 0.0;
                    }
                
                    if(dj == 1){
                        mc = 0.0;
                        qdc = 0.0;
                        
                        if(di != i && s != 1){
                            if(di != 0 && s != -1){
                                cu = 3;
                            }
                        }
                    }
                
                    if(dj == j-1){

                        ms = 0.0;
                        qcc = j;

                        if(di != i && di != 1){
                            if(di != 0 && di != -1){
                                cu = 4;
                            }
                        }

                    }
                
                    pau = vx[(int)mu][dj];
                    peu = vx[(int) mu][(int) ms];
                    piu = vx[(int) mu][(int) mc];
                    pad = vy[di][(dj + s)];
                    ped = vy[(int) mt][(dj + s)];
                    pid = vy[(int) md][(dj + s)];
                    rau = a[(n + 1)][(int) mu][dj];
                    reu = a[(n + z)][(int) md][dj];
                    riu = a[(n + 1)][(int) mu][(int) ms];
                    rou = a[(n + 1)][(int) mu][(int) mc];
                    rad = a[(n + 1)][di][(dj + s)];
                    red = a[(n + z)][di][(int) mc];
                    rid = a[(n + 1)][(int) mt][(dj + s)];
                    rod = a[(n + 1)][(int) md][(dj + s)];
                    qa = a[(n + 1)][(int) qac][dj];
                    qb = a[(n + 1)][(int) qbc][dj];
                    qc = a[(n + 1)][di][(int) qcc];
                    qd = a[(n + 1)][di][(int) qdc];
            
                ///////////////////////////////////////////////
                
                    if(cu == 1) {
                        pau = vx[(i - 1)][dj] - vy[i][(dj + 1)] + vy[(int) i][(dj - 1)];
		
                        if(dj != (j - 1) && dj != (j - 2)) {
                          peu = vx[(i - 1)][(dj + 2)] - vy[i][(dj + 3)] + vy[i][(dj + 1)];
                        }
		
                        if(dj != 1 && dj != 2) {
                          piu = vx[(int)(i - 1)][(dj - 2)] - vy[i][dj - 1] + vy[i][(dj - 3)];
                        }
		
                        if( dj == j - 2) {
                          peu = vx[(i - 1)][j];
                        }
		
                        if(dj == 2) {
                          piu = vx[(i - 1)][0];
                        }
		
                        if(dj == 1 || (double) dj == j - 1) {
                          peu = 0.0;
                          piu = 0.0;
                        }
		
                        rau = 8.99f;
                        riu = 8.99f;
                        rou = 8.99f;
                    }
		
                    if(cu == 2) {
                      pau = vx[1][dj] + vy[0][(dj + 1)] - vy[0][(dj - 1)];
		
                        if(dj != j - 1 &&  dj != j - 2) {
                          peu = vx[1][(dj + 2)] + vy[0][(dj + 3)] - vy[0][(dj + 1)];
                        }

                        if(dj != 1 && dj != 2) {
                          piu = vx[1][(dj - 2)] + vy[0][(dj - 1)] - vy[0][(dj - 3)];
                        }

                        if((double) dj == j - 2) {
                          peu = vx[1][j];
                        }
		
                        if(dj == 2) {
                          piu = vx[1][0];
                        }
		
                        if(dj == 1 || dj == j - 1) {
                          peu = 0.0;
                          piu = 0.0;
                        }
				
                        rau = 10.01f;
                        riu = 10.01f;
                        rou = 10.01f;
                    }
	      
                    if(di == 0) {
                      pid = ped;
                      reu = 10.02f;
                      rod = 10.02f;
                      qb = 10.01f;
                    }
		
                    if(di == 1) {
                      pid = ped;
                      reu = 10.01f;
                      rod = 10.01f;
                    }
	      
                    if(di == i) {
                      ped = pid;
                      rid = 8.98f;
                      qa = 8.99f;
                    }
	      
                    if(di == i - 1) {
                      ped = pid;
                      rid = 8.99f;
                    }
		
                    if(cu == 3) {
                      piu = 0.0;
                      rou = a[(n + 1)][(int) mu][1];
                      red = a[(n + z)][di][1];
                    }
	      
                    if(cu == 4) {
                      peu = 0.0;
                      riu = a[(n + 1)][(int)mu][(j - 1)];
                    }
	      
	      /* Calculation of the potencial */

                    ca = -s * ex * (pau * (rau * 8 - reu) 
                                    + (riu - rou) * (peu - piu) / 8);
                    cb = -s * ye * (pad * (rad * 8 - red) 
                                    + (rid - rod) * (ped - pid) / 8);
                    fa = fa + ca + cb;
                    cd = 1 - cs * (ex * pau + ye * pad) + cs * s * (l + m);

                    if(cs == 1) {
                      if(s == -1) {
                        cda = cd;
                      } else {
                        cdb = cd;
                      }
                    }
	      
                    if(cs == -1) {
                      if(s == 1) {
                        cda = cd;
                      } else {
                        cdb = cd;
                      }
                    }
                }
              
                cc = l * (qa + qb) + m * (qc + qd);
                fe = fa + cc;
                fi = fe + a[n][di][dj] * cda;
                a[(n + 2)][di][dj] = fi / cdb;
              
	    } 
        }
        
        for(di = 0; di <= i; di++){
            a[n+2][di][0] = a[n][di][0];
            a[n+2][di][j] = a[n][di][j];
        }
        
    }
    char str1[15]= "PCELL";
    char str2[15]= ".DAT";
    char num[12];
    sprintf(num, "%d", nf);
    strcat(str1, num);
    strcat(str1, str2);
    
    FILE *pcell;
    
    pcell = fopen(str1, "w");
    
    for(di = 0; di <= i; di++){
    
        for(dj = 0; dj <= j; dj++){
        
            test1[di][dj] = a[n][di][dj];
            test[di][dj] = a[n+1][di][dj];
            
            fprintf(pcell, "%f \n", a[n+1][di][dj]);
            
                if(dj == j){
                 fprintf(pcell, "\n");
                }
        }

    }
    
    fclose(pcell);
        
}
    


